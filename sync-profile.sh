#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Synchronize profile v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Synchronize KDE profile"
PROFILEDIR=/home/roelof/Repos/kavaliros-kde
PROFILEWORKDIR=/mnt/ramdisk/profile
sudo chown roelof:users $PROFILEWORKDIR/packages.x86_64
sudo chown roelof:users $PROFILEWORKDIR/pacman.conf
cat $PROFILEDIR/kde-packages.x86_64 >> $PROFILEWORKDIR/packages.x86_64
sudo rsync -avzh --exclude '.git/*' --exclude '.git*' --exclude 'sync-profile.sh' --exclude 'kde-packages.x86_64' $PROFILEDIR/ $PROFILEWORKDIR/
