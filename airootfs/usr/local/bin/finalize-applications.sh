echo "Finalize Applications" >> /var/log/finalize-applications.log

# Remove Calamares icon form all desktops
if [ -f /home/$1/Desktop/calamares.desktop ]; then
    rm /home/$1/Desktop/calamares.desktop >> /var/log/finalize-applications.log
fi

#Enable kernel update
systemctl enable efi-update.path >> /var/log/finalize-applications.log

#Enable NTP
timedatectl set-ntp true >> /var/log/finalize-applications.log

#Update user ID for same id as synaology nas
usermod -u 1026 $1 >> /var/log/finalize-applications.log

# Boot EFI
cp /boot/vmlinuz-linux /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log
cp /boot/initramfs-linux.img /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log
cp /boot/amd-ucode.img /boot/efi/KavalirOS/ >> /var/log/finalize-applications.log

#Micro code
sed -i '/^[[:blank:]]*#/d' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log
sed -i '/^[[:space:]]*$/d' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log
sed -i '2 a initrd /KavalirOS/amd-ucode.img' /boot/efi/loader/entries/KavalirOS.conf >> /var/log/finalize-applications.log

#NVidia
if [ -f /usr/bin/nvidia-xconfig ]; then
    /usr/bin/nvidia-xconfig >> /var/log/finalize-applications.log
fi
