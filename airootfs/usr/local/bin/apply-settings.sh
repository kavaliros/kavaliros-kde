echo "Apply application settings " >> /var/log/apply-settings.log

# Set default cursor to Bibata-Modern-Amber
sed -i s/Adwaita/Bibata-Modern-Amber/g /usr/share/icons/default/index.theme >> /var/log/apply-settings.log

# Setup scanner
if [ -f /usr/bin/brsaneconfig4 ]; then
    brsaneconfig4 -a name=Brother model=j4610dw ip=192.168.178.38 >> /var/log/apply-settings.log
fi

# VirtualBox
if [ -f /usr/bin/virtualbox ]; then
    gpasswd -a $1 vboxusers >> /var/log/apply-settings.log
fi

if [ -f /usr/lib/modules-load.d/virtualbox-guest-dkms.conf ]; then
    gpasswd -a $1 vboxsf >> /var/log/apply-settings.log
    systemctl enable vboxservice >> /var/log/apply-settings.log
fi

# VMware
if [ -f /etc/pam.d/vmtoolsd ]; then
    systemctl enable data-shared.mount >> /var/log/apply-settings.log
else
    systemctl disable data-shared.mount >> /var/log/apply-settings.log
    rm /etc/systemd/system/data-shared.mount >> /var/log/apply-settings.log
fi

# SyncThing
if [ -f usr/bin/syncthing ]; then
    systemctl enable syncthing@$1 >> /var/log/apply-settings.log
fi

# NordVPN
if [ -f /usr/bin/nordvpn ]; then
    systemctl enable nordvpnd >> /var/log/apply-settings.log
    gpasswd -a $1 nordvpn >> /var/log/apply-settings.log
fi

# ICAclient. 
if [ -d /opt/Citrix/ICAClient ]; then
    tar xvf /etc/calamares/appsettings/ICAClient.tar.gz -C /home/$1 >> /var/log/apply-settings.log
fi

# NetExtender
if [ -f /usr/sbin/pppd ]; then
    chmod u+s /usr/sbin/pppd >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/netextender.tar.gz -C /home/$1 >> /var/log/apply-settings.log
fi

#Displaylink
if [ -f /usr/lib/systemd/system/displaylink.service ]; then
    systemctl enable displaylink >> /var/log/apply-settings.log
fi

#Dotnet
if [ -d /usr/share/dotnet ]; then
    dotnet tool install --global dotnet-dev-certs >> /var/log/apply-settings.log
    dotnet tool install --global dotnet-ef >> /var/log/apply-settings.log
    echo 'export PATH="$PATH:/home/username/.dotnet/tools"' | sudo tee /etc/profile.d/dotnet.sh 
    sed -i s/username/$1/g /etc/profile.d/dotnet.sh >> /var/log/apply-settings.log
    dotnet dev-certs https >> /var/log/apply-settings.log
fi

#Visual Studio Code OSS
if [ -f /usr/bin/code-oss ]; then
    tar xvf /etc/calamares/appsettings/vscode-oss.tar.gz -C /home/$1 >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/Code\ -\ OSS.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Visual Studio Code 
if [ -f /opt/visual-studio-code/bin/code ]; then
    tar xvf /etc/calamares/appsettings/vscode.tar.gz -C /home/$1 >> /var/log/apply-settings.log
    tar xvf /etc/calamares/appsettings/Code.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Libre Office
if [ -f /usr/bin/libreoffice ]; then
    tar xvf /etc/calamares/appsettings/libreoffice.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Calibre
if [ -f /usr/bin/calibre ]; then
    tar xvf /etc/calamares/appsettings/calibre.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Gscan2Pdf
if [ -f /usr/bin/gscan2pdf ]; then
    tar xvf /etc/calamares/appsettings/gscan2pdfrc.tar.gz -C /home/$1/.config >> /var/log/apply-settings.log
fi

#Disable auto-login
sed -i "s/User=kavaliro/User=/g" /etc/sddm.conf.d/kde_settings.conf >> /var/log/apply-settings.log

# Remove settings
rm -Rf /etc/calamares/appsettings >> /var/log/apply-settings.log

# Set sudo time out to 30 minutes
sed '$ a Defaults:$1 timestamp_timeout=30' /etc/sudoers