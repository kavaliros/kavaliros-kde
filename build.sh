#!/bin/bash
#  __  __                     __ __        _______ _______ 
# |  |/  |.---.-.--.--.---.-.|  |__|.----.|       |     __|
# |     < |  _  |  |  |  _  ||  |  ||   _||   -   |__     |
# |__|\__||___._|\___/|___._||__|__||__|  |_______|_______|
#
#  Program :	Make KavalirOS KDE ISO v1
#  Arch    :	x86_64 
#  Author  : 	Roelof Ridderman
#

function banner() {
	term_cols=$(tput cols) 
	str=":: $1 ::"
	for ((i=1; i<=`tput cols`; i++)); do echo -n ‾; done
	tput setaf 10; echo "$str";  tput sgr0
	for ((i=1; i<=`tput cols`; i++)); do echo -n _; done
}

banner "Make new KavalirOS KDE ISO"
PROFILE=kde
WORKDIR=/mnt/ramdisk/work
PROFILEWORKDIR=/mnt/ramdisk/profile
PROFILEDIR=/usr/share/archiso/configs/releng
PACMANCONFIG=/home/roelof/Repos/kavaliros-kde/pacman.conf
CALAMARESDIR=/home/roelof/Repos/kavaliros-calamares
ISODIR=/home/roelof/Shared
banner "Create Work folder"
sudo mount -t ramfs -o size=8G ramfs /mnt/ramdisk
sudo mkdir /mnt/ramdisk/work
sudo mkdir /mnt/ramdisk/profile
rm $ISODIR/KavalirOS-$PROFILE-x86_64.iso
banner "Update system"
sudo pacman -Syy
sudo pacman -Syu
banner "Synchronize base profile"
sudo rsync -avzh $PROFILEDIR/ $PROFILEWORKDIR/
bash ./sync-profile.sh
bash $CALAMARESDIR/sync.sh
banner "Make ISO $PROFILE"
sudo sed -i "2 a VERSION=$PROFILE" $PROFILEWORKDIR/profiledef.sh 
sudo bash ./fixmkarchiso.sh -C $PACMANCONFIG  -v -w $WORKDIR -o $ISODIR $PROFILEWORKDIR
set -- $ISODIR/KavalirOS-$PROFILE-x86_64.iso
banner "$1 created" 
banner "Unmount RAM disk"
sudo umount /mnt/ramdisk
